
/** 
 * main.java
 *
 * Title:			Stair Runner
 * Description:	    Karel climbs the stairs and removes the beepers
 * @author			Aidan Kahrs
 * @version			
 */

import kareltherobot.*;

public class StairRunner implements Directions 
{

	public static void task() 
	{
	   StairSweeper karel = new StairSweeper (1,1,East,1);
	   	 karel.climbStair();
	   	 karel.climbStair();
	   	 karel.climbStair();
	   	 karel.turnLeft();
         karel.turnOff();
		
	} // task 

	// Main entry point
	public static void main(String[] args) 
     {
		World.setDelay(50);
		World.readWorld("stairs.txt");
		World.setVisible();
		task();
	}
}

