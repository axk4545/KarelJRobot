
/** 
 * main.java
 *
 * Title:			
 * Description:	    
 * @author			
 * @version			
 */

import kareltherobot.*;

public class KarelMain implements Directions 
{

	public static void task() 
	{
		UrRobot karel = new UrRobot (6,6,South,1);
		karel.move();
		karel.move();
		karel.move();
		karel.turnLeft();
		karel.turnLeft();
		karel.turnLeft();
		karel.move();
		karel.move();
		karel.move();
		karel.move();
		karel.move();
		karel.pickBeeper();
		karel.turnLeft();
		karel.turnLeft();
		karel.turnLeft();
		karel.putBeeper();
		karel.move();
		karel.putBeeper();
		karel.move();
        karel.turnOff();
		
	} // task 

	// Main entry point
	public static void main(String[] args) 
     {
		World.setDelay(50);
		World.readWorld("oneBeeper.TXT");
		World.setVisible();
		task();
	}
}

