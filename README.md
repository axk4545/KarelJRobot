KarelJRobot
===========
My projects using the KarelJRobot tool to create simple, entertaining Java programs.
KarelJRobot files can be found at http://csis.pace.edu/~bergin/KarelJava2ed/downloads.html
