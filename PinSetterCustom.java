
/** 
 * main.java
 *
 * Title:		PinSetterCustom	
 * Description:	half pyramid pattern w/ beepers 
 * @author			Aidan Kahrs
 * @version		1	
 */

import kareltherobot.Directions;
import kareltherobot.World;
//import kareltherobot.Robot;

public class PinSetterCustom implements Directions 
{

	public static void task() 
	{
		PinSetterC karel = new PinSetterC (1,5,North,10);
		karel.move();
		karel.move();
		karel.move();
		karel.move();
		karel.turnLeft();
		karel.move();
		karel.move();
		karel.move();// move to (5,1)
		
		karel.halfTurn();
		karel.moveB();
		karel.moveC();
		karel.moveC();
		karel.turnRight();//end row 1
		karel.move();
		karel.turnRight();
		karel.moveB();
		karel.move();
		karel.move();
		karel.turnLeft();
		karel.putBeeper();
		karel.move();
		karel.turnLeft();//end row 2
		karel.move();
		karel.move();
		karel.moveB();
		karel.turnRight();
		karel.move();
		karel.turnRight();
		karel.move();//end row 3
		karel.putBeeper();
		karel.turnLeft();
		karel.move();
		karel.turnLeft();
		karel.turnLeft();
        karel.turnOff();//end row 4
		
	} // task 

	// Main entry point
	public static void main(String[] args) 
     {
		World.setDelay(50);
		/*World.readWorld("oneBeeper.TXT");*/
		World.setVisible();
		task();
	}
}

