
/** 
 * KarelBigMove.java
 *
 * Title:	KarlBigMove		
 * Description:	 Move beeper to 3,5   
 * @author	Aidan Kahrs		
 * @version	10/21/13		
 */

import kareltherobot.*;

public class KarelBigMove implements Directions 
{

	public static void task() 
	{
		UrRobot karel = new UrRobot (1,2,East,0);
			karel.turnLeft();
			karel.turnLeft();
			karel.move();
			karel.turnLeft();
			karel.turnLeft();
			karel.turnLeft();
			karel.move();
			karel.move();
			karel.pickBeeper();
			karel.turnLeft();
			karel.turnLeft();
			karel.turnLeft();
			karel.move();
			karel.move();
			karel.move();
			karel.move();
			karel.putBeeper();
			karel.turnLeft();
			karel.move();
			karel.turnOff(); // street:4 avenue:5 facing North
		
	} // task 

	// Main entry point
	public static void main(String[] args) 
     {
		World.setDelay(50);
		World.readWorld("oneBeeper.TXT");
		World.setVisible();
		task();
	}
}
