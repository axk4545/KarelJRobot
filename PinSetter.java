
/** 
 * main.java
 *
 * Title:		PinSetter	
 * Description:	Bowling pin pattern w/ beepers    
 * @author			Aidan Kahrs
 * @version		1	
 */

import kareltherobot.*;

public class PinSetter implements Directions 
{

	public static void task() 
	{
		UrRobot karel = new UrRobot (1,5,North,10);
		karel.move();
		karel.move();
		karel.move();
		karel.move();
		karel.turnLeft();
		karel.move();
		karel.move();
		karel.move();
		karel.turnLeft();
		karel.turnLeft();
		karel.putBeeper();
		karel.move();
		karel.move();
		karel.putBeeper();
		karel.move();
		karel.move();
		karel.putBeeper();
		karel.move();
		karel.move();
		karel.putBeeper();
		karel.turnLeft();
		karel.turnLeft();
		karel.turnLeft();
		karel.move();
		karel.turnLeft();
		karel.turnLeft();
		karel.turnLeft();
		karel.move();
		karel.putBeeper();
		karel.move();
		karel.move();
		karel.putBeeper();
		karel.move();
		karel.move();
		karel.turnLeft();
		karel.putBeeper();
		karel.move();
		karel.turnLeft();
		karel.move();
		karel.putBeeper();
		karel.move();
		karel.move();
		karel.putBeeper();
		karel.turnLeft();
		karel.turnLeft();
		karel.turnLeft();
		karel.move();
		karel.turnLeft();
		karel.turnLeft();
		karel.turnLeft();
		karel.move();
		karel.putBeeper();
		karel.turnLeft();
		karel.move();
		karel.turnLeft();
		karel.turnLeft();
        karel.turnOff();
		
	} // task 

	// Main entry point
	public static void main(String[] args) 
     {
		World.setDelay(50);
		/*World.readWorld("oneBeeper.TXT");*/
		World.setVisible();
		task();
	}
}

