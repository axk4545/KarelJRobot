
/** 
 * main.java
 *
 * Title:	Karel2Robots		
 * Description:	    Karel trades a beeper with Jane
 * @author			Aidan Kahrs
 * @version			1
 */

import kareltherobot.*;

public class Karel2Robots implements Directions 
{

	public static void task() 
	{
		UrRobot karel = new UrRobot (3,1,East,1);
		UrRobot jane = new UrRobot (1,1,East,0);
		karel.turnLeft();
		karel.turnLeft();
		karel.turnLeft();
		karel.move();
		karel.move();
		karel.putBeeper();
		karel.turnLeft();
		karel.turnLeft();
		karel.turnOff();
		
		jane.pickBeeper();
		jane.move();
		jane.move();
		jane.putBeeper();
		jane.move();
		jane.turnLeft();
		jane.turnOff();
        
		
	} // task 
	
	// Main entry point
	public static void main(String[] args) 
     {
		World.setDelay(50);
		//World.readWorld("oneBeeper.TXT");
		World.setVisible();
		task();
	}
}

